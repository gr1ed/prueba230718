<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'AppliancesController@index')->name('appliances');
Route::post('/add', 'AppliancesController@add')->name('appliances.add');


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home', 'WishesController@index')->name('home');
Route::post('/home/remove', 'WishesController@remove')->name('wishes.delete');
Route::post('/home/share', 'WishesController@share')->name('wishes.share');
