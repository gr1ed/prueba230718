<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Goutte;
use App;

class UpdateAppliances extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'appliances:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update appliances of the site with data of (https://www.appliancesdelivered.ie/search/small-appliances?sort=price_desc) and (https://www.appliancesdelivered.ie/dishwashers)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Begin to scrape the first url

        $this->scrape(env('DISHWASHERS_APPLIANCES_URL'));

        //Begin to scrape the second url

        $this->scrape(env('SMALL_APPLIANCES_URL'));
    }

    public static function scrape($url)
    {
        $crawler = Goutte::request('GET', $url);

        $pages = 0;

        if($crawler->filter('.result-list-pagination a')->count() > 0)
        {
            $lastPageLink = $crawler->filter('.result-list-pagination a:last-child')->attr('href');
            $ex_lastPageLink = explode('=', $lastPageLink);
            $pages = end($ex_lastPageLink);
        }

        for ($i = 0; $i < $pages; $i++)
        {
            if ($i != 0)
            {
                $crawler = Goutte::request('GET', $url.'&page='.($i + 1));
            }

            $crawler->filter('.search-results-product')->each(function ($node)
            {
                $full_name = $node->filter('h4')->text();
                $name = $full_name;
                $ex_sku = explode(' ', $full_name);
                $sku = end($ex_sku);

                $image = $node->filter('.product-image img:first-child')->attr('src');

                $price = $node->filter('.section-title')->text();

                $saleprice = ($node->filter('.price-previous')->count() > 0) ? $node->filter('.price-previous')->text() : null;

                $brandimage = ($node->filter('.article-brand')->count() > 0) ? $node->filter('.article-brand')->attr('src') : null;

                $a_attributes = $node->filter('.result-list-item-desc-list li')->each(function ($li)
                {
                    return $attributes[] = $li->text();
                });

                $s_attributes = implode(',', $a_attributes);

                $appliance = App\Appliance::where('sku', $sku)->first();

                if($appliance)
                {
                    $appliance->name = $name;
                    $appliance->sku = $sku;
                    $appliance->image = $image;
                    $appliance->price = (float)str_replace(",", "", str_replace("€", "", $price));
                    $appliance->saleprice = $saleprice;
                    $appliance->attributes = $s_attributes;
                    $appliance->brandimage = $brandimage;

                    $appliance->save();
                }
                else
                {
                    $o_appliance = new App\Appliance;

                    $o_appliance->name = $name;
                    $o_appliance->sku = $sku;
                    $o_appliance->image = $image;
                    $o_appliance->price = (float)str_replace(",", "", str_replace("€", "", $price));
                    $o_appliance->saleprice = $saleprice;
                    $o_appliance->attributes = $s_attributes;
                    $o_appliance->brandimage = $brandimage;

                    $o_appliance->save();
                }
            });
        }
        return true;
    }
}
