<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App;

class AppliancesController extends Controller
{
    /**
     * Show the appliances grid
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sort = Request::get('sort');

        if($sort)
        {
            if($sort == "title_desc")
            {
                $appliances = App\Appliance::orderBy('name', 'desc')->paginate(20);
            }
            else if($sort == "title_asc")
            {
                $appliances = App\Appliance::orderBy('name', 'asc')->paginate(20);
            }
            else if($sort == "price_asc")
            {
                $appliances = App\Appliance::orderBy('price', 'asc')->paginate(20);
            }
            else
            {
                $appliances = App\Appliance::orderBy('price', 'desc')->paginate(20);
            }
        }
        else
        {
            $appliances = App\Appliance::orderBy('price', 'desc')->paginate(20);
        }


        return view('appliances', ['appliances' => $appliances]);
    }

    public function add()
    {
        $user_id = Request::get('id_user');
        $appliance_id = Request::get('id_appliance');

        $wish = App\Wish::where('user_id', $user_id)->where('appliance_id', $appliance_id)->first();

        if(!$wish)
        {
            $o_wish = new App\Wish;

            $o_wish->user_id = $user_id;
            $o_wish->appliance_id = $appliance_id;

            $o_wish->save();
        }

        return back()->with('status', 'Appliance added to wishlist');
    }
}
