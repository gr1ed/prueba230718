<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App;
use Auth;

class WishesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the appliances grid
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $wishes = App\Wish::where('user_id', $user->id)->paginate(20);

        return view('wishes', ['wishes' => $wishes]);
    }

    public function remove()
    {
        $wish_id = Request::get('id_wish');

        App\Wish::destroy($wish_id);

        return back()->with('status', 'Appliance deleted from wishlist');
    }

    public function share()
    {
        $email = Request::get('email');

        $user = Auth::user();

        $friend = App\User::where('email', $email)->first();

        if($friend)
        {
            $wishes = App\Wish::where('user_id', $user->id)->get();

            if($wishes)
            {
                foreach ($wishes as $wish)
                {
                    $q_wish = App\Wish::where('user_id', $friend->id)->where('appliance_id', $wish->appliance_id)->first();

                    if(!$q_wish)
                    {
                        $o_wish = new App\Wish;

                        $o_wish->user_id = $friend->id;
                        $o_wish->appliance_id = $wish->appliance_id;

                        $o_wish->save();
                    }
                }
            }
            return back()->with('status', 'Wishlist shared');
        }
        return back()->with('status', "Can't share the wishlist the user don't exist");
    }
}
