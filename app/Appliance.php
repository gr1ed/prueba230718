<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appliance extends Model
{
    protected $fillable = [
        'name', 'sku', 'image', 'price', 'saleprice', 'attributes', 'brandimage'
    ];
}
