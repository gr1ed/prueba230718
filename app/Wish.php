<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wish extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function appliance()
    {
        return $this->belongsTo('App\Appliance');
    }
}
