@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Wishlist</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-md-12" style="margin-bottom:30px">
                        <form method="POST" action="{{ route('wishes.share') }}">
                            {{ csrf_field() }}
                            <div class="col-md-10">
                                <input id="email" type="text" name="email" class="form-control" value="{{ old('email') }}">
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-primary">Share wishlist</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12">
                        @if (count($wishes) != 0)
                            @foreach ($wishes as $wish)
                            <div class="col-md-3" style="height:650px">
                                <img src="{{ $wish->appliance->image }}" alt="{{ $wish->appliance->name }}">
                                @if ($wish->appliance->brandimage)
                                <img src="{{ $wish->appliance->brandimage }}" style="max-height:30px" alt="{{ $wish->appliance->name }}">
                                @endif
                                <div class="col-md-12" style="height:90px">{{ $wish->appliance->name }}</div>
                                @if ($wish->appliance->saleprice)
                                    <div class="col-md-6">€{{ $wish->appliance->price }}</div>
                                    <div class="col-md-6" style="text-decoration:line-through">{{ $wish->appliance->saleprice }}</div>
                                @else
                                    <div class="col-md-12">€{{ $wish->appliance->price }}</div>
                                @endif
                                @php
                                    $s_attributes = $wish->appliance->attributes;
                                    $a_attributes = explode(",", $s_attributes);
                                @endphp
                                <div class="col-md-12" style="height:120px">
                                    <ul>
                                        @foreach ($a_attributes as $attribute)
                                            <li>{{ $attribute }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <form method="POST" action="{{ route('wishes.delete') }}">
                                    {{ csrf_field() }}
                                    <input class="id_wish" type="hidden" name="id_wish" value="{{ $wish->id }}">
                                    <button type="submit" class="btn btn-danger">Delete from wishlist</button>
                                </form>
                            </div>
                            @endforeach
                            {{ $wishes->links() }}
                        @else
                            <span>Wish list empty</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
