@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Small appliances and dishwashers</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-md-12" style="margin-bottom:30px">
                        <div class="col-md-3">
                            <a href="{{ route('appliances', ['sort' => 'price_asc']) }}" class="btn btn-primary" role="button">Order by price asc</a>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ route('appliances', ['sort' => 'price_desc']) }}" class="btn btn-primary" role="button">Order by price desc</a>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ route('appliances', ['sort' => 'title_asc']) }}" class="btn btn-primary" role="button">Order by title asc</a>
                        </div>
                        <div class="col-md-3">
                            <a href="{{ route('appliances', ['sort' => 'title_desc']) }}" class="btn btn-primary" role="button">Order by title desc</a>
                        </div>
                    </div>
                    <div class="col-md-12">
                        @foreach ($appliances as $appliance)
                        <div class="col-md-3" style="height:650px">
                            <img src="{{ $appliance->image }}" alt="{{ $appliance->name }}">
                            @if ($appliance->brandimage)
                            <img src="{{ $appliance->brandimage }}" style="max-height:30px" alt="{{ $appliance->name }}">
                            @endif
                            <div class="col-md-12" style="height:90px">{{ $appliance->name }}</div>
                            @if ($appliance->saleprice)
                                <div class="col-md-6">€{{ $appliance->price }}</div>
                                <div class="col-md-6" style="text-decoration:line-through">{{ $appliance->saleprice }}</div>
                            @else
                                <div class="col-md-12">€{{ $appliance->price }}</div>
                            @endif
                            @php
                                $s_attributes = $appliance->attributes;
                                $a_attributes = explode(",", $s_attributes);
                            @endphp
                            <div class="col-md-12" style="height:120px">
                                <ul>
                                    @foreach ($a_attributes as $attribute)
                                        <li>{{ $attribute }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @auth
                                <form method="POST" action="{{ route('appliances.add') }}">
                                    {{ csrf_field() }}
                                    <input class="id_appliance" type="hidden" name="id_appliance" value="{{ $appliance->id }}">
                                    <input class="id_user" type="hidden" name="id_user" value="{{ Auth::user()->id }}">
                                    <button type="submit" class="btn btn-primary">Add to wishlist</button>
                                </form>
                            @endauth
                            @guest
                                <a href="{{ route('login') }}" class="btn btn-primary" role="button">Add to wishlist</a>
                            @endguest
                        </div>
                        @endforeach
                        {{ $appliances->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
